import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public alertCtrl:AlertController) {

  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Hello!',
      message: 'We are still working on it',
      buttons: [
        {
          text: 'Click here to close'
        }
      ]
    });
    alert.present();
  }
}
